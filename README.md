# NXEngine-evo
toki ante pi nasin NXEngine-evo

sitelen Lasina en sitelen Ilakana en sitelen pona li lon.

[README.en.md (English)](README.en.md)

README.jp.md (日本語)

[![Latest Release](https://gitlab.com/lupa-Tokusu-Monokatali/musi-Tokusu-Monokatali/-/badges/release.svg)](https://gitlab.com/lupa-Tokusu-Monokatali/musi-Tokusu-Monokatali/-/releases)

## make pana

```
git clone https://gitlab.com/lupa-Tokusu-Monokatali/nxengine-evo.git
cd nxengine-evo
```

o kama jo e ilo [NXEngine-evo](https://github.com/nxengine/nxengine-evo/releases)

o ante e POKI_LIPU lon lipu [Makefile](Makefile?plain=18#L18-22)

```
# Game directory
POKI_MUSI := $(wildcard ~/.local/share/nxengine/nxengine-evo)# Linux
# POKI_MUSI := $(wildcard ~/snap/nxengine-evo/current/.local/share/nxengine)# Linux (installed with `snap install nxengine-evo`)
# POKI_MUSI := $(wildcard ~/Library/Application\ Support/nxengine/nxengine-evo)# Macintosh
# POKI_MUSI := $(wildcard ~/Library/Application\ Data/nxengine/nxengine-evo)# Windows
```

o kepeken wan e ni:

- `make pana LANG=en` sitelen Lasina taso
- `make pana LANG=jp` sitelen Ilakana taso
- `make pana LANG=tp` sitelen pona taso
- `make pana LANG=` ale

o `make musi`

## pana luka

ilo supa la, o kama jo e NXEngine-evo: [https://github.com/nxengine/nxengine-evo/releases](https://github.com/nxengine/nxengine-evo/releases)

nasin Snap la: `sudo snap install nxengine-evo`

ilo Switch la:
[https://www.gamebrew.org/wiki/NXEngine-evo_Switch](https://www.gamebrew.org/wiki/NXEngine-evo_Switch)

o kama jo e lipu, o open e ona. o pana e poki `lang` lon poki `data/lang`

ilo Linux (tan nasin Snap)
: `/home/<username>/snap/nxengine-evo/current/.local/share/nxengine/data/lang/`

ilo Linux
: `/home/<username>/.local/share/nxengine/data/lang/`

ilo Macintosh
: `/Users/<username>/Library/Application\ Support/nxengine/data/lang/`

ilo Windows
: `/users/<username>/Library/Application\ Data/nxengine/data/lang/`

ilo Switch
: `/switch/nxengine/data/lang/`

## o ante e toki

![](sitelen/nxengine-evo-lang-01.png "o open e nasin")
o open e "nasin"

![](sitelen/nxengine-evo-lang-03.png "o noka e nena ⬅ anu nena ➡")
o tawa "toki". o noka e nena ⬅ anu nena ➡. o ante e ona.
