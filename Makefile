# o toki ala e ni
MAKEFLAGS += --no-print-directory

DATESTAMP := $(shell date --iso-8601)

# Main project directory
# POKI_LAWA = .
ifneq ($(wildcard ../../.gitmodules),)
# $(info Found ../../.gitmodules)
# $(info $(shell grep "nasin/nxengine-evo$$" "../../.gitmodules"))
ifeq ($(shell grep "nasin/nxengine-evo$$" "../../.gitmodules"),)
POKI_LAWA = .
else
POKI_LAWA = ../..
endif

else
# $(info Could not find ../../.gitmodules)
POKI_LAWA = .
endif

POKI_ANTE = $(POKI_LAWA)/ante

# Build directory
POKI_PALI := $(POKI_LAWA)/pali/nxengine-evo

# Dist directory
POKI_PINI  = $(POKI_LAWA)/pini
LIPU_PINI  = nxengine-evo-toki-pona-$(DATESTAMP).zip

# Game directory
POKI_MUSI := $(wildcard ~/.local/share/nxengine/nxengine-evo)# Linux
# POKI_MUSI := $(wildcard ~/snap/nxengine-evo/current/.local/share/nxengine)# Linux (installed with `snap install nxengine-evo`)
# POKI_MUSI := $(wildcard ~/Library/Application\ Support/nxengine/nxengine-evo)# Macintosh
# POKI_MUSI := $(wildcard ~/Library/Application\ Data/nxengine/nxengine-evo)# Windows

# Install directory
ifdef POKI_MUSI
POKI_PANA := $(POKI_MUSI)/data/lang/
endif

.PRECIOUS: $(POKI_MUSI)/toki-pona_profile.dat  \
           $(POKI_MUSI)/toki-pona_profile1.dat \
           $(POKI_MUSI)/toki-pona_profile2.dat \
           $(POKI_MUSI)/toki-pona_profile3.dat \
           $(POKI_MUSI)/toki-pona_profile4.dat \
           $(POKI_MUSI)/toki-pona_profile5.dat \
           $(POKI_MUSI)/toki-pona_profile6.dat

.PHONY: all       ale ali	\
	build     pali		\
	dist      pini		\
	install   pana		\
        uninstall		\
	clean			\
	weka			\
	run       musi		\

all: pali
ale: pali
ali: pali



build: pali
pali: $(POKI_PALI)/data/lang/toki-pona $(POKI_PALI)/data/lang/sitelen-Ilakana $(POKI_PALI)/data/lang/sitelen-pona $(POKI_PALI)/README.md $(POKI_PALI)/README.en.md

$(POKI_PALI)/data/lang/toki-pona: $(POKI_ANTE)/toki-pona
	mkdir -p $(POKI_PALI)/data/lang/toki-pona
	cp -r $(POKI_ANTE)/toki-pona/data/* $(POKI_PALI)/data/lang/toki-pona
	cp -r lipu/ale/* $(POKI_PALI)/data/lang/toki-pona
	cp -r lipu/toki-pona/* $(POKI_PALI)/data/lang/toki-pona

$(POKI_PALI)/data/lang/sitelen-Ilakana: $(POKI_ANTE)/sitelen-Ilakana
	mkdir -p $(POKI_PALI)/data/lang/sitelen-Ilakana
	cp -r $(POKI_ANTE)/sitelen-Ilakana/data/* $(POKI_PALI)/data/lang/sitelen-Ilakana
	cp -r lipu/ale/* $(POKI_PALI)/data/lang/sitelen-Ilakana
	cp -r lipu/sitelen-Ilakana/* $(POKI_PALI)/data/lang/sitelen-Ilakana

$(POKI_PALI)/data/lang/sitelen-pona: $(POKI_ANTE)/sitelen-pona
	mkdir -p $(POKI_PALI)/data/lang/sitelen-pona
	cp -r $(POKI_ANTE)/sitelen-pona/data/* $(POKI_PALI)/data/lang/sitelen-pona
	cp -r lipu/ale/* $(POKI_PALI)/data/lang/sitelen-pona
	cp -r lipu/sitelen-pona/* $(POKI_PALI)/data/lang/sitelen-pona

$(POKI_PALI)/README.md: README.md
	cp README.md $@

$(POKI_PALI)/README.en.md: README.en.md
	cp README.en.md $@

dist: pini
pini: pali $(POKI_PINI)/$(LIPU_PINI)
$(POKI_PINI)/$(LIPU_PINI): $(POKI_PINI)/ lipu/*
ifdef ($@)
	rm -f $@
endif
	cd $(POKI_PALI); zip -r $(LIPU_PINI) * 1> /dev/null
	mv $(POKI_PALI)/$(LIPU_PINI) $(POKI_PINI)/$(LIPU_PINI)
	@echo 'pini pona'
$(POKI_PINI)/:
	mkdir -p $@


install: pana
pana: pali $(POKI_PALI) $(POKI_MUSI)
ifdef POKI_MUSI
	mkdir -p $(POKI_PANA)
	rsync -rpeU $(POKI_PALI)/data/lang/ $(POKI_PANA)
	@echo "toki ante li pini pana pona"
else
	$(error poki musi li lon ala.  ijo NXEngine-evo li lon ala lon?)
endif



clean:
	rm -rf $(POKI_PALI)
ifdef ($(POKI_PINI)/nxengine-evo-toki-pona-$(DATESTAMP).zip)
	rm -f $(POKI_PINI)/nxengine-evo-toki-pona-$(DATESTAMP).zip
endif

uninstall:
ifdef POKI_MUSI
	rm -rf $(POKI_PANA)/toki-pona
	rm -rf $(POKI_PANA)/sitelen-pona
	rm -rf $(POKI_PANA)/sitelen-Ilakana
	@echo "Uninstalled from:" $(POKI_PANA)
else
	$(error "Could not find nxengine-evo")
endif

weka: clean uninstall
ifdef POKI_MUSI
	@echo "toki ante li pini weka"
endif



ante/toki-pona:
	git submodule update --init --rebase --remote
ante/sitelen-Ilakana:
	git submodule update --init --rebase --remote
ante/sitelen-pona:
	git submodule update --init --rebase --remote



run: musi
musi: pana
	@cd $(POKI_MUSI); ./nx
musi-snap: musi-senapu
musi-senapu:
	snap run nxengine-evo



$(HOME)/snap/nxengine-evo:
	@echo "lanpan e musi NXEngine-evo..."
	@sudo snap install nxengine-evo
