# NXEngine-evo
Translation pack for NXEngine-evo

Includes Latin, Hiragana, and sitelen pona.

[README.md (toki pona)](README.md)

README.jp.md (日本語)

[![Latest Release](https://gitlab.com/lupa-Tokusu-Monokatali/musi-Tokusu-Monokatali/-/badges/release.svg)](https://gitlab.com/lupa-Tokusu-Monokatali/musi-Tokusu-Monokatali/-/releases)

## make install

```
git clone https://gitlab.com/lupa-Tokusu-Monokatali/nxengine-evo.git
cd nxengine-evo
```

Download [NXEngine-evo](https://github.com/nxengine/nxengine-evo/releases)

Set game directory in [Makefile](Makefile?plain=18#L18-22)

```
# Game directory
POKI_MUSI := $(wildcard ~/.local/share/nxengine/nxengine-evo)# Linux
# POKI_MUSI := $(wildcard ~/snap/nxengine-evo/current/.local/share/nxengine)# Linux (installed with `snap install nxengine-evo`)
# POKI_MUSI := $(wildcard ~/Library/Application\ Support/nxengine/nxengine-evo)# Macintosh
# POKI_MUSI := $(wildcard ~/Library/Application\ Data/nxengine/nxengine-evo)# Windows
```

Use one of the following:

- `make install LANG=en` sitelen Lasina only
- `make install LANG=jp` sitelen Ilakana only
- `make install LANG=tp` sitelen pona only
- `make install LANG=` all

Run: `make run`

## Manual Installation

Download NXEngine-evo for Desktop: [https://github.com/nxengine/nxengine-evo/releases](https://github.com/nxengine/nxengine-evo/releases)

Snap install: `sudo snap install nxengine-evo`

Switch: [https://www.gamebrew.org/wiki/NXEngine-evo_Switch](https://www.gamebrew.org/wiki/NXEngine-evo_Switch)

Unzip and copy `lang` folder into `data/lang`

Linux (snap)
: `/home/<username>/snap/nxengine-evo/current/.local/share/nxengine/data/lang/`

Linux
: `/home/<username>/.local/share/nxengine/data/lang/`

Macintosh
: `/Users/<username>/Library/Application\ Support/nxengine/data/lang/`

Windows
: `/users/<username>/Library/Application\ Data/nxengine/data/lang/`

Switch
: `/switch/nxengine/data/lang/`

## Set language

![](sitelen/nxengine-evo-lang-01.png "Open Options")

Open "Options"



![](sitelen/nxengine-evo-lang-03.png "Press Left or Right to change.")

Go to "Language".  Press Left or Right to change.
